package scraper

import (
	"fmt"
	"sync"
)

type Job struct {
	responseChan chan *Page
	url          string
	host         string
}

type Octopus struct {
	scrapedLinks map[string]bool
	mutex        sync.Mutex
}

func NewOctopus() *Octopus {
	scraped := make(map[string]bool)
	mutex := sync.Mutex{}
	return &Octopus{scrapedLinks: scraped, mutex: mutex}
}

func (o *Octopus) Start(workerCount int) chan *Job {
	jobChan := make(chan *Job)
	w := Werker{jobChan: jobChan}
	go func() {
		for job := range jobChan {
			if o.shouldScrape(job.url) {
				fmt.Printf("Scraping: %s\n", job.url)
				w.Werk(job)
			} else {
				w.Reject(job)
			}
		}
	}()

	return jobChan
}

func (o *Octopus) shouldScrape(uri string) bool {
	return !o.hasScraped(uri)
}

func (o *Octopus) hasScraped(uri string) bool {
	o.mutex.Lock()
	if _, ok := o.scrapedLinks[uri]; ok {
		// If we have the url stored, tell them not to scrape it
		o.mutex.Unlock()
		return true
	}

	// Otherwise add it to the map and tell them it's free
	o.scrapedLinks[uri] = true
	o.mutex.Unlock()
	return false
}

type Werker struct {
	jobChan chan *Job
}

func (w *Werker) Werk(job *Job) {
	page, err := NewPage(job.host, job.url)
	if err != nil {
		fmt.Printf("Error creating new page from URL %s => %s \n", job.url, err)
		job.responseChan <- nil
		close(job.responseChan)
	}

	go func() {
		page.Fetch()
		page.ScrapeChildren(w.jobChan)
		job.responseChan <- page

		close(job.responseChan)
	}()
}

func (w *Werker) Reject(job *Job) {
	go func() {
		job.responseChan <- nil
		close(job.responseChan)
	}()
}
