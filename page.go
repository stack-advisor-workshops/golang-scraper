package scraper

import (
	"fmt"
	"net/url"
	"strings"
)

type Page struct {
	IsRoot      bool
	Title       string
	Content     string
	Description string
	Links       []string
	Url         *url.URL
	Host        string
	Images      []string
	Pages       []*Page
}

func NewPage(host string, uri string) (*Page, error) {
	uriData, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}

	return &Page{Host: host, Url: uriData, IsRoot: false}, nil
}

// ToPageList takes all nested pages and makes a flat list out of them
func (p *Page) ToPagelist() []*Page {
	pages := []*Page{}
	if p.IsRoot {
		pages = append(pages, p)
	}

	for _, subPage := range p.Pages {
		pages = append(pages, subPage)
		for _, page := range subPage.ToPagelist() {
			pages = append(pages, page)
		}
	}
	return pages
}

// Fetch just a delegate to the public Scrape function, there is probably a good reason for this.
func (p *Page) Fetch() error {
	return Scrape(p)
}

// ScrapeChildren starts worker jobs for each of the page's Links
// and waits for the responses to all of them before returning
// It updates the page in place
func (p *Page) ScrapeChildren(jobChan chan *Job) {
	receiveChans := []chan *Page{}

	for _, link := range p.Links {
		receiveChan := make(chan *Page)
		receiveChans = append(receiveChans, receiveChan)
		go func(rChan chan *Page, l string) {
			job := Job{responseChan: rChan, url: l, host: p.Host}
			jobChan <- &job
		}(receiveChan, link)
	}

	var pages []*Page
	for _, receiveChan := range receiveChans {
		page := <-receiveChan
		if page != nil {
			pages = append(pages, page)
		}
	}
	p.Pages = pages
	return
}

// IndexPages search indexes a list of pages into solr
func IndexPages(coreName string, siteID string, pages []*Page) error {
	searchClient, err := NewSearchClient(coreName)
	if err != nil {
		return err
	}

	fmt.Printf("Site ID: %s\n", siteID)

	docs := []map[string]interface{}{}

	for _, page := range pages {
		fmt.Printf("Indexing %s\n", page.Title)
		doc := map[string]interface{}{
			"title":       page.Title,
			"content":     page.Content,
			"description": page.Description,
			"url":         page.Url.String(),
			"site_id":     strings.ToLower(siteID),
		}
		docs = append(docs, doc)
	}

	err = searchClient.UpdateMany("page", docs)
	if err != nil {
		return fmt.Errorf("Error indexing page %s\n", err)
	}

	return nil
}
