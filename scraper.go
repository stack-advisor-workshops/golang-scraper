package scraper

import (
	"log"
)

func Start(rootURL string) *Page {
	octo := NewOctopus()
	jobChan := octo.Start(2)

	page, err := NewPage(rootURL, rootURL)
	if err != nil {
		log.Fatal(err)
	}
	page.IsRoot = true

	page.Fetch()
	page.ScrapeChildren(jobChan)
	return page
}
