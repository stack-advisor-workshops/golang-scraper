module github.com/mraaroncruz/scraper

go 1.15

require (
	github.com/advancedlogic/GoOse v0.0.0-20200830213114-1225d531e0ad
	github.com/antchfx/htmlquery v1.2.3
	github.com/pkg/errors v0.8.1
	github.com/rtt/Go-Solr v0.0.0-20190512221613-64fac99dcae2
)
