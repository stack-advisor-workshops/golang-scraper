package scraper

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"regexp"

	goose "github.com/advancedlogic/GoOse"
	"github.com/antchfx/htmlquery"
	"github.com/pkg/errors"
)

// Scrape grabs HTML from a URL, then parses content and links out of it
func Scrape(page *Page) error {
	html, err := getHTML(page.Url.String())
	if err != nil {
		return err
	}

	err = getLinks(page.Host, html, page)
	if err != nil {
		return err
	}

	err = getContent(html, page)
	if err != nil {
		return err
	}

	return nil
}

func getHTML(uri string) ([]byte, error) {
	resp, err := http.Get(uri)
	if err != nil {
		return nil, err
	}

	contentType := resp.Header.Get("Content-Type")
	htmlHeaderMatcher := regexp.MustCompile(`text/html`)
	if !htmlHeaderMatcher.MatchString(contentType) {
		return nil, errors.Errorf("Incompatible content type %s", contentType)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func getLinks(host string, html []byte, page *Page) error {
	reader := bytes.NewReader(html)
	doc, err := htmlquery.Parse(reader)
	if err != nil {
		return err
	}

	list := htmlquery.Find(doc, "//a/@href")
	for _, linkDoc := range list {
		link := htmlquery.SelectAttr(linkDoc, "href")
		urlData := newURLData(host, link)
		if urlData.isValid() {
			page.Links = append(page.Links, urlData.AbsoluteURL())
		}
	}

	return nil
}

func getContent(html []byte, page *Page) error {
	g := goose.New()
	article, err := g.ExtractFromRawHTML(string(html), page.Url.String())
	if err != nil {
		return err
	}

	page.Title = article.Title
	page.Description = article.MetaDescription
	page.Content = article.CleanedText
	page.Images = []string{article.TopImage}

	return nil
}
