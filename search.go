package scraper

import (
	solr "github.com/rtt/Go-Solr"
)

type SearchClient struct {
	client *solr.Connection
}

func NewSearchClient(core string) (*SearchClient, error) {
	// init a connection
	s, err := solr.Init("localhost", 8983, core)

	if err != nil {
		return nil, err
	}

	return &SearchClient{client: s}, nil
}

func (s *SearchClient) Update(docType string, doc map[string]interface{}) error {
	return s.UpdateMany(docType, []map[string]interface{}{doc})
}

func (s *SearchClient) UpdateMany(docType string, docs []map[string]interface{}) error {
	// build an update document, in this case adding two documents
	for i := range docs {
		docs[i]["type"] = docType
	}

	f := map[string]interface{}{
		"add": docs,
	}

	// send off the update (2nd parameter indicates we also want to commit the operation)
	_, err := s.client.Update(f, true)

	if err != nil {
		return err
	}

	return nil
}

func (s *SearchClient) Search(docType string, query string) ([]map[string]interface{}, error) {
	q := solr.Query{
		Params: solr.URLParamMap{
			"q":           []string{query},
			"facet.field": []string{"type", docType},
		},
		Rows: 100,
	}

	res, err := s.client.Select(&q)

	if err != nil {
		return nil, err
	}

	results := res.Results
	docs := results.Collection
	items := []map[string]interface{}{}
	for _, doc := range docs {
		items = append(items, doc.Fields)
	}
	return items, nil
}
