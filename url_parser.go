package scraper

import (
	"net/url"
	"strings"
)

type URLData struct {
	host      string
	urlString string
	url       *url.URL
	err       error
}

func newURLData(host string, link string) *URLData {
	parsedUrl, err := url.Parse(link)
	if err != nil {
		return &URLData{err: err, urlString: link, host: host}
	}

	return &URLData{url: parsedUrl, urlString: link, host: host}
}

func (u *URLData) isValid() bool {
	if u.err != nil {
		return false
	}

	if u.url.Path == "" {
		return false
	}

	if u.host == u.url.Host {
		return true
	}

	if u.url.Host == "" && u.url.Path != "" {
		return true
	}

	return false
}

func (u *URLData) AbsoluteURL() string {
	uri := u.url.String()
	if u.url.IsAbs() {
		return uri
	}

	path := uri
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}

	host, _ := url.Parse(u.host)
	newURI := host.Scheme + "://" + host.Host + path
	return newURI
}
